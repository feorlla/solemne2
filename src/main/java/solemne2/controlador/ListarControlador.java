/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solemne2.controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import solemne2.dao.EstudiantesJpaController;
import solemne2.dao.exceptions.NonexistentEntityException;
import solemne2.entities.Estudiantes;

/**
 *
 * @author GorillaSetups
 */
@WebServlet(name = "ListarControlador", urlPatterns = {"/ListarControlador"})
public class ListarControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListarControlador</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListarControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EstudiantesJpaController dao = new EstudiantesJpaController();
        List<Estudiantes> listaEstudiantes = dao.findEstudiantesEntities();
        request.setAttribute("listaEstudiantes", listaEstudiantes);
        request.getRequestDispatcher("listarAlumno.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String boton = request.getParameter("accion");
        if (boton.equals("cancelar")) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        if (boton.equals("modificar")) {
            try {
                String rut = request.getParameter("rut");
                String nombre = request.getParameter("nombre");
                String carrera = request.getParameter("carrera");
                String correo = request.getParameter("correo");
                String telefono = request.getParameter("telefono");
                Estudiantes estudiantes = new Estudiantes();
                estudiantes.setRut(rut);
                estudiantes.setNombre(nombre);
                estudiantes.setCarrera(carrera);
                estudiantes.setCorreo(correo);
                estudiantes.setTelefono(telefono);
                EstudiantesJpaController dao = new EstudiantesJpaController();
                dao.edit(estudiantes);
                List<Estudiantes> listaEstudiantes = dao.findEstudiantesEntities();
                request.setAttribute("listaEstudiantes", listaEstudiantes);
                request.getRequestDispatcher("listarAlumno.jsp").forward(request, response);
            } catch (Exception ex) {
                Logger.getLogger(ListarControlador.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (boton.equals("eliminar")) {
            try {
                String rut = request.getParameter("seleccion");
                EstudiantesJpaController dao = new EstudiantesJpaController();
                dao.destroy(rut);
                List<Estudiantes> listaEstudiantes = dao.findEstudiantesEntities();
                request.setAttribute("listaEstudiantes", listaEstudiantes);
                request.getRequestDispatcher("listarAlumno.jsp").forward(request, response);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ListarControlador.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        if (boton.equals("editar")) {
            String rut = request.getParameter("seleccion");
            EstudiantesJpaController dao = new EstudiantesJpaController();
            Estudiantes estudiantes = dao.findEstudiantes(rut);
            request.setAttribute("estudiantes", estudiantes);
            request.getRequestDispatcher("editarAlumno.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
