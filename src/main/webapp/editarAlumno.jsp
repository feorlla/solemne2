<%-- 
    Document   : editarAlumno
    Created on : 01-06-2021, 23:32:24
    Author     : GorillaSetups
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="solemne2.entities.Estudiantes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    
  Estudiantes estudiantes=(Estudiantes)request.getAttribute("estudiantes");
    
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Editar Alumno</h1>
        
        <form action="ListarControlador" method="POST">     
                    
        Rut Alumno: <input type="text" name="rut" readonly value="<%= estudiantes.getRut() %>" /><br>
        Nombre Alumno: <input type="text" name="nombre" value="<%= estudiantes.getNombre() %>" /><br>
        Carrera: <input type="text" name="carrera" value="<%= estudiantes.getCarrera() %>" /><br>
        Mail: <input type="text" name="correo" value="<%= estudiantes.getCorreo() %>" /><br>
        Telefono: <input type="text" name="telefono" value="<%= estudiantes.getTelefono()%>" /><br>
        
        <input type="submit" value="modificar" name="accion" />
        
        </form>
    </body>
</html>
