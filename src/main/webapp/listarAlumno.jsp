<%-- 
    Document   : editarAlumno
    Created on : 31-05-2021, 20:45:53
    Author     : GorillaSetups
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="solemne2.entities.Estudiantes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    
    List<Estudiantes> lista = (List<Estudiantes>) request.getAttribute("listaEstudiantes");
    Iterator<Estudiantes> itEstudiantes = lista.iterator();
    
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       
        <form action="ListarControlador" method="POST"> 
         <%if (lista != null) {%>
       <table border="1" width="650" align="center" >
           <tr>
               <td colspan="6" align="center">
                   <h3>Listado de Alumnos:</h3> 
               </td>
           </tr>
                <tr>
                        <td>RUT</td>
                        <td>NOMBRE</td>
                        <td>CARRERA</td>
                        <td>MAIL</td>
                        <td>TELEFONO</td>
                       <td>ACCION</td>
                </tr>
                    <% while (itEstudiantes.hasNext()) {
                        Estudiantes estudiantes = itEstudiantes.next();
                    %>
                    <tr>
                        <td><%= estudiantes.getRut() %></td>
                        <td><%= estudiantes.getNombre() %></td>
                        <td><%= estudiantes.getCarrera() %></td>
                        <td><%= estudiantes.getCorreo() %></td>
                        <td><%= estudiantes.getTelefono()%></td>
                        <td><input type="radio" name="seleccion" value="<%= estudiantes.getRut() %>" /> </td> 
                    </tr>
                   <%}%>  
            <tr>
            <td colspan="2" align="center"><input type="submit" name="accion" value="editar" /></td>
            <td  colspan="2" align="center" ><input type="submit" name="accion" value="eliminar" /></td>
            <td  colspan="2" align="center" ><input type="submit" name="accion" value="cancelar" /></td>
             </tr>
       
       </table>
             <%}%>  
            
        </form>
    </body>
</html>
